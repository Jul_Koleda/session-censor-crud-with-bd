<!DOCTYPE html>
<html lang="en">
<head>

    <link href="bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="styles/style.css" rel="stylesheet">

    <meta charset="UTF-8">
    <!--    <title>Create form with pure PHP (No Symfony)</title>-->

</head>

<body>
<h2>Registration</h2>

<div class='bd-example'>
    <form method="post" action="reg_form_action.php">

        <div class="form-group">
            <label for="name">
                <input type="text" name="name" placeholder="name" id="nm"/>
            </label>
            <label for="password">
                <input type="password" name="password" placeholder="password" id="pw2"/>
            </label>
            <button type="submit" class="btn btn-success" href="main.php">Send</button>
        </div>

    </form>

</div>
</body>